﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] GameObject loadingText;
    [SerializeField] GameObject startGameButton;

    [SerializeField] float timeToWait = 1;

    int currentSceneIndex;

    void Start()
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            
        StartCoroutine(WaitForTime(timeToWait));
    }

    IEnumerator WaitForTime(float timeToWait)
    {
        yield return new WaitForSeconds(timeToWait);
        loadingText.SetActive(false);
        startGameButton.SetActive(true);
    }

    void Update()
    {
        
    }
}
